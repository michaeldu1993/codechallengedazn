
#!/bin/bash

set +e

svcHost=127.0.0.1:4000
testUser=testuser
maxCalls=250

if [ ! -z ${1} ]; then
    svcHost=${1}
fi

if [ ! -z ${2} ]; then
    testUser=${2}
fi

if [ ! -z ${3} ]; then
    maxCalls=${3}
fi

curlOptions="-o /dev/null -s -w %{http_code} -H userid:${testUser}"

callCount=0
redirectCount=0
while [ ${callCount} -lt ${maxCalls} ]; do
    if [ $(curl ${curlOptions} ${svcHost}/stream) -eq 302 ]; then
        #echo "Call ${callCount}: redirected"
        redirectCount=$((redirectCount+1))
    #else
        #echo "Call ${callCount}: failed"
    fi

    callCount=$((callCount+1))
done

echo "Total calls: ${callCount}, Total redirects: ${redirectCount}"

