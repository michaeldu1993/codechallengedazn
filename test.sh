
#!/bin/bash

set +e

svcHost=127.0.0.1:4000

if [ ! -z ${1} ]; then
    svcHost=${1}
fi

curlOptions="-o /dev/null -s -w %{http_code}"

runTest() {
    userId=${1}
    testApi=${2}
    statusCode=${3}
    testCase=${4}

    if [ $(curl ${curlOptions} -H "userid:${userId}" ${svcHost}${testApi}) -eq ${statusCode} ]; then
        echo "[Passed] ${testCase}"
    else
        echo "[Failed] ${testCase}"
    fi
}

# Define test users
validUserId1='test'
validUserId2='123'
validUserId3='test123'
invalidUserId1='teST'
invalidUserId2='12THREE'
invalidUserId3='test-123'
redirectUser='testuser100'
streamCountUser='testuser200'
streamLimitUser='testuser300'

# Define test cases
testStreamApi=/stream

# Create test data, user / concurrent streams
redis-cli SET ${validUserId1} 0
redis-cli SET ${validUserId2} 0
redis-cli SET ${validUserId3} 0
redis-cli SET ${invalidUserId1} 0
redis-cli SET ${invalidUserId2} 0
redis-cli SET ${invalidUserId3} 0
redis-cli SET ${redirectUser} 0
redis-cli SET ${streamCountUser} 2
redis-cli SET ${streamLimitUser} 3

# Run test cases
runTest "${validUserId1}" "${testStreamApi}" 302 "test valid user id 1"
runTest "${validUserId2}" "${testStreamApi}" 302 "test valid user id 2"
runTest "${validUserId3}" "${testStreamApi}" 302 "test valid user id 3"
runTest "${invalidUserId1}" "${testStreamApi}" 400 "test invalid user id 1"
runTest "${invalidUserId2}" "${testStreamApi}" 400 "test invalid user id 2"
runTest "${invalidUserId3}" "${testStreamApi}" 400 "test invalid user id 3"
runTest "404usernotfound" "${testStreamApi}" 404 "test non existent user"
runTest "${redirectUser}" "${testStreamApi}" 302 "test redirect"
runTest "${streamCountUser}" "${testStreamApi}" 302 "test stream count 1"
runTest "${streamCountUser}" "${testStreamApi}" 403 "test stream count 2"
runTest "${streamLimitUser}" "${testStreamApi}" 403 "test stream limit"

# Create test data, user / concurrent streams
redis-cli DEL ${validUserId1}
redis-cli DEL ${validUserId2}
redis-cli DEL ${validUserId3}
redis-cli DEL ${invalidUserId1}
redis-cli DEL ${invalidUserId2}
redis-cli DEL ${invalidUserId3}
redis-cli DEL ${redirectUser}
redis-cli DEL ${streamCountUser}
redis-cli DEL ${streamLimitUser}

