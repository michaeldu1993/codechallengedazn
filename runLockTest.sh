
#!/bin/bash

set +e

svcHost=127.0.0.1:4000
testUser=testuser
maxCalls=200

if [ ! -z ${1} ]; then
    svcHost=${1}
fi

if [ ! -z ${2} ]; then
    testUser=${2}
fi

if [ ! -z ${3} ]; then
    maxCalls=${3}
fi

redis-cli SET ${testUser} 0

resultsFile="./results"

echo "" > ${resultsFile}
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &
./lockTest.sh ${svcHost} ${testUser} ${maxCalls} >> ${resultsFile} &

wait

echo ""
redis-cli GET ${testUser}

cat ${resultsFile}

redis-cli DEL ${testUser}

