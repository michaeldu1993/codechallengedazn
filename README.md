
--

-------------------------------
 DAZN code challenge
-------------------------------

Service that prevents users from watching more than 3 video streams at the same time.
Expose an api for any cilent to consume.



-------------------------------
Install Node JS
-------------------------------

sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm



-------------------------------
Install Redis Server
-------------------------------

sudo apt-get update
sudo apt-get install redis-server



-------------------------------
Install Node JS Packages
-------------------------------

cd codechallengedazn/service
npm install



-------------------------------
Run server
-------------------------------

cd codechallengedazn/service
node service.js



-------------------------------
Run test
-------------------------------

Default host: 127.0.0.1, port: 4000

cd codechallengedazn/
./test.sh
or
./test.sh <host>
e.g.  ./test.sh https://solution.dev.mdu.cloud
e.g.  ./test.sh 127.0.0.1:4000



-------------------------------
Test Data
-------------------------------

To create a user, use the following command:
redis-cli SET <userid> <number of concurrent streams>



-------------------------------
Test Redis Locking
-------------------------------

1. Edit codechallengedazn/service/config.json
   change streamLimit from 3 to 1000

   This is to simulate multiple api calls trying to set a value in redis,
   increase the streamLimit allows more calls to be made to see the result

2. cd codechallengedazn/service
   node service.js
   or
   node service.js | grep attempt
   to see the retries

3. Open another terminal
   cd codechallengedazn/

   Default host: 127.0.0.1:4000, testuser: testuser, maxcalls: 200

   ./runLockTest.sh
   or
   ./runLockTest.sh <host> <test user> <max calls>
   e.g.  ./runLockTest.sh https://solution.dev.mdu.cloud testuser 200
   e.g.  ./runLockTest.sh 127.0.0.1:4000 testuser 200

   This script runs 6 background tasks, each making <max calls> api calls
   to <host> with user <test user>.

   With the stream limit set to 1000, and 6 background tasks doing 200 calls
   each (1200 calls total), only 1000 calls will be redirected, meaning that
   the locking works



-------------------------------
Online URL
-------------------------------

https://solution.dev.mdu.cloud/stream

test users available:
testuser1
testuser2
testuser3
testuser4
testuser5
testuser6
testuser7
testuser8
testuser9
testuser10
these users should all have 0 concurrent streams



-------------------------------
Scalability
-------------------------------

Redis can scale vertically and horizontally by increasing the performance
of the machine the redis instance is deployed on and adding more redis
instances and set up replication between them. On average a key pair use
around 30 bytes. It will be able to support millions of key pairs.

The nodejs server can scale both vertically and horizontally as well,
by deploying it on a more powerful machine and having multiple instances of it.

In the scenario when a user is already watching 2 streams, and two api calls
are made, which ever call gets processed first will go through and the other
will be blocked (node redis locking).



-------------------------------
API
-------------------------------

This api uses redis to store userid and the amount of stream they are watching as key value pairs.
When the api is called by a client, it will check whether userid is valid (contains lower case and
numbers only), then create a connection to redis, check whether the userid exist in redis. If it
does, it will get the amount of streams the user is currently watching, if this number is less than
3, it will add one to it and set the new value in redis, locking is in place to make sure the key
value has not been changed between getting the value and setting the new value. The client is then
redirected to the stream. If there has been a change between getting a value and setting a value,
it will retry the whole process 3 times before returning try again later to the client.

API: /stream HEADER userid:<userid>
usage example: /stream HEADER userid:testuser

Below are all the differnt outcomes:
1. 404 is returned if the user does not exist
2. 403 is returned if the user is already watching 3 or more streams
   (or if the server is unable to determine whether the user can watch anymore streams)
3. 400 is returned if the userid is invalid, format (lower case and numbers only)
4. 302 is returned if the user is redirected to the stream (this could return a 200 with
   a token that the client can use with another api to watch the stream)

This api does not require authentication, which should be done in a real world scenario.
For example an (/login) api can be added to return a session token for /stream to use
The api checks whether the userid exist, this is only in place in the absence of (/login) api

With an (/login) api, the behaviour would be changed from:
If userid exist in redis, then get value, then set new value.
If userid doesn't exist in redis, return 404
to
If userid doesn't exist in redis, set the new value, as it will be 0, no need to get
Since it will already be authenticated through (/login)

