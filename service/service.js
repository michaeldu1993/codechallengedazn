
const express = require("express");
const app = express();

const http = require("http");
const httpServer = http.createServer(app);

const config = require("./config.json");

const httpPort = config.httpPort;

const redisHost = config.redisHost;
const redisPort = config.redisPort;

const redis = require("redis");

function errLog(err) {
    if (err) {
        console.log(err);
    }
}

const ipRegex = /[0-9\.]+/g;
const userIdRegex = /^[a-z0-9]+$/;

const streamLimit = config.streamLimit;
const redirectLink = config.redirectLink;
const maxAttempts = config.maxAttempts;

function processRequest(res, time, host, path, ip, userId, currentAttempt, redisClient) {
    redisClient.exists(userId, (err, result) => {
        errLog(err);
        // Check user id exist
        if (result) {
            // Check whether another key set has occured between this key get and set
            redisClient.watch(userId, (err) => {
                errLog(err);
                redisClient.get(userId, (err, result) => {
                    errLog(err);
                    let numOfStreams = parseInt(result);

                    // Check user is not already watching 3 or more streams
                    if (numOfStreams < streamLimit) {
                        numOfStreams++;
                        redisClient.multi().set(userId, numOfStreams).exec( (err, result) => {
                            errLog(err);
                            // Check whether key has been set successfully
                            if (result) {
                                res.redirect(redirectLink);
                                console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, redirected`);
                                redisClient.quit();
                            }
                            else {
                                // Allow 3 retries before returning to client
                                if (currentAttempt < maxAttempts) {
                                    console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, redis set key attempt ${currentAttempt}`);
                                    currentAttempt++;
                                    processRequest(res, time, host, path, ip, userId, currentAttempt, redisClient);
                                }
                                else {
                                    res.status(403);
                                    res.send(`Please try again later\n`);
                                    console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, max redis set key attempts reached, try later`);
                                    redisClient.quit();
                                }
                            }
                        });
                    }
                    else {
                        res.status(403);
                        res.send(`You can not watch more than ${streamLimit} streams\n`);
                        console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, stream limit reached`);
                        redisClient.quit();
                    }
                });
            });
        }
        else {
            res.status(404);
            res.send(`UserID: ${userId} does not exist\n`);
            console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, UserID does not exist`);
            redisClient.quit();
        }
    });
}

app.get("/stream", (req, res, next) => {
    const time = new Date();
    const host = req.hostname;
    const path = req.path;
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress.match(ipRegex)[0];
    const userId = req.header("userid");

    // Check user has provided userId header
    if (userId) {
        // Check user id format
        if (userIdRegex.test(userId)) {
            const redisClient = redis.createClient(redisPort, redisHost);

            //redisClient.on("connect", () => {
            //    console.log(`[Info] Connected to redis server at ${redisHost}:${redisPort}`);
            //});

            // Log when redis client reconnects
            redisClient.on("error", () => {
                console.log(`[Error] Unable to connect to redis server at ${redisHost}:${redisPort}`);
                redisClient.on("connect", () => {
                    console.log(`[Info] Reconnected to redis server at ${redisHost}:${redisPort}`);
                });
            });

            const currentAttempt = 1;
            processRequest(res, time, host, path, ip, userId, currentAttempt, redisClient);
        }
        else {
            res.status(400);
            res.send(`UserID: ${userId} invalid\n`);
            console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, UserID invalid`);
        }
    }
    else {
        res.status(400);
        res.send(`Header "userid" required\n`);
        console.log(`${time}, ${host}, ${path}, ${ip}, userid: ${userId}, UserID undefined`);
    }
});

httpServer.listen(httpPort, () => {
    console.log(`Http server running on port ${httpPort}`);
});

